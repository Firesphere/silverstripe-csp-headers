---
title: Documentation
footer: false
---

# Documentation

This is an automatically generated documentation for **Firesphere\CSPHeaders**.


## Namespaces


### \Firesphere\CSPHeaders\Admins

#### Classes

| Class | Description |
|---    |---          |
| [SRIAdmin](./classes/Firesphere/CSPHeaders/Admins/SRIAdmin.md) | Class \Firesphere\CSPHeaders\Admins\SRIAdmin|




### \Firesphere\CSPHeaders\Builders

#### Classes

| Class | Description |
|---    |---          |
| [BaseBuilder](./classes/Firesphere/CSPHeaders/Builders/BaseBuilder.md) | Class Firesphere\CSPHeaders\Builders|
| [CSSBuilder](./classes/Firesphere/CSPHeaders/Builders/CSSBuilder.md) | Class Firesphere\CSPHeaders\Builders\CSSBuilder|
| [JSBuilder](./classes/Firesphere/CSPHeaders/Builders/JSBuilder.md) | Class Firesphere\CSPHeaders\Builders\JSBuilder|
| [SRIBuilder](./classes/Firesphere/CSPHeaders/Builders/SRIBuilder.md) | Class Firesphere\CSPHeaders\Builders\SRIBuilder|




### \Firesphere\CSPHeaders\Extensions

#### Classes

| Class | Description |
|---    |---          |
| [CSPBuildExtension](./classes/Firesphere/CSPHeaders/Extensions/CSPBuildExtension.md) | Class \Firesphere\CSPHeaders\Extensions\CSPBuildExtension|
| [ControllerCSPExtension](./classes/Firesphere/CSPHeaders/Extensions/ControllerCSPExtension.md) | Class \Firesphere\CSPHeaders\Extensions\ControllerCSPExtension|
| [PageExtension](./classes/Firesphere/CSPHeaders/Extensions/PageExtension.md) | Class \Firesphere\CSPHeaders\Extensions\SiteTreeExtension|




### \Firesphere\CSPHeaders\Helpers

#### Classes

| Class | Description |
|---    |---          |
| [CSPConvertor](./classes/Firesphere/CSPHeaders/Helpers/CSPConvertor.md) | Class \Firesphere\CSPHeaders\Helpers\CSPConvertor|




### \Firesphere\CSPHeaders\Interfaces




#### Interfaces

| Interface | Description |
|---    |---          |
| [BuilderInterface](./classes/Firesphere/CSPHeaders/Interfaces/BuilderInterface.md) | Interface Firesphere\CSPHeaders\Interfaces\BuilderInterface|



### \Firesphere\CSPHeaders\Models

#### Classes

| Class | Description |
|---    |---          |
| [CSPDomain](./classes/Firesphere/CSPHeaders/Models/CSPDomain.md) | Class \Firesphere\CSPHeaders\Models\CSPDomain|
| [SRI](./classes/Firesphere/CSPHeaders/Models/SRI.md) | Class \Firesphere\CSPHeaders\Models\SRI|




### \Firesphere\CSPHeaders\Tasks

#### Classes

| Class | Description |
|---    |---          |
| [SRIRefreshTask](./classes/Firesphere/CSPHeaders/Tasks/SRIRefreshTask.md) | Class Firesphere\CSPHeaders\Tasks\SRIRefreshTask|




### \Firesphere\CSPHeaders\Traits



#### Traits

| Trait | Description |
|---    |---          |
| [CSPBackendTrait](./classes/Firesphere/CSPHeaders/Traits/CSPBackendTrait.md) | Trait Firesphere\CSPHeaders\Traits\CSPBackendTrait|




### \Firesphere\CSPHeaders\View

#### Classes

| Class | Description |
|---    |---          |
| [CSPBackend](./classes/Firesphere/CSPHeaders/View/CSPBackend.md) | Class Firesphere\CSPHeaders\View\Backend|




---
> Automatically generated from source code comments on 2023-11-14 using [phpDocumentor](http://www.phpdoc.org/) and [dmarkic/phpdoc3-template-md](https://github.com/dmarkic/phpdoc3-template-md)
