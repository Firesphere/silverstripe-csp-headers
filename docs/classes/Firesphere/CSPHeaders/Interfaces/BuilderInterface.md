---
title: \Firesphere\CSPHeaders\Interfaces\BuilderInterface
footer: false
---

# BuilderInterface

Interface Firesphere\CSPHeaders\Interfaces\BuilderInterface



* Full name: `\Firesphere\CSPHeaders\Interfaces\BuilderInterface`
* Parent interface: [](../../../../classes.md)



## Methods

### buildTags



```php
public BuilderInterface::buildTags(string $file, array $attributes, array $requirements, string $path): array
```








**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `file` | **string** |  |
| `attributes` | **array** |  |
| `requirements` | **array** |  |
| `path` | **string** |  |


**Return Value:**





---
### getCustomTags



```php
public BuilderInterface::getCustomTags( $requirements): array
```








**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `requirements` | **** |  |


**Return Value:**





---
### getHeadTags



```php
public BuilderInterface::getHeadTags(array& $requirements): void
```








**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `requirements` | **array** |  |


**Return Value:**





---


---
> Automatically generated from source code comments on 2023-11-14 using [phpDocumentor](http://www.phpdoc.org/) and [dmarkic/phpdoc3-template-md](https://github.com/dmarkic/phpdoc3-template-md)
